Diffusion Limited Aggregation

----------------------------------------------------------------------------------------------------

Description:

A C program to generate an animated, randomised DLA using the ncurses library.

----------------------------------------------------------------------------------------------------

Pre-requisites:

ncurses library

----------------------------------------------------------------------------------------------------

Compile options:

gcc dla.c neillncurses.c -Wall -Wfloat - equal -Wextra -O2
-pedantic -ansi -lncurses -lm