/* Note: Structs implemented to test structs,
   probably more efficient/readable without
   in this case. */

#include "dla.h"

void particle(struct walker *pedge);
void propagate(char grid[][GRIDLEN], struct walker *pwalk);
int negmod(int tomod);

int main(void)
{
   int seed, i, j, k;
   char grid[GRIDLEN][GRIDLEN];
   struct walker ptcle;
   NCURS_Simplewin sw;

   seed = time(NULL);
   srand(seed);

   Neill_NCURS_Init(&sw);
   Neill_NCURS_CharStyle(&sw, "#", COLOR_BLACK, COLOR_RED, A_INVIS);
   
   /* Fairly hungry loop algorithm to continually propagate 
      new DLA clusters, min. On^3 */
   do{
      for (i=0; i<GRIDLEN; i++){
         ptcle.y = i;
         for (j=0; j<GRIDLEN; j++){
            ptcle.x = j;
            grid[ptcle.y][ptcle.x] = ' ';
         }
      }

      grid[GRIDLEN/2][GRIDLEN/2] = '#';
      
      for(k=0; k<MAXPTCLES; k++){
         particle(&ptcle);
         propagate(grid, &ptcle);
         Neill_NCURS_PrintArray(&grid[0][0], GRIDLEN, GRIDLEN, &sw);
         Neill_NCURS_Delay(10.0);
         Neill_NCURS_Events(&sw);
      }

      k = 0;

   }while(!sw.finished);
   
   atexit(Neill_NCURS_Done);
   exit(EXIT_SUCCESS);

   return 0;
}

/* Create a particle on a
   random edge of a grid. */
void particle(struct walker *pedge)
{
   int edge, edgepos;

   edge = rand() % EDGES;
   edgepos = rand() % (GRIDLEN - 1);

   switch(edge){
      case 0:
         pedge->x = 0;
         pedge->y = edgepos;
      break;
      case 1:
        pedge->x = GRIDLEN - 1;
        pedge->y = edgepos;
      break;
      case 2:
         pedge->x = edgepos;
         pedge->y = 0;
      break;
      case 3:
         pedge->x = edgepos;
         pedge->y = GRIDLEN - 1;
      break;
   }
}

/* Random-walk current particle. Check ahead, stop
   and remain in place if cluster reached. */
void propagate(char grid[][GRIDLEN], struct walker *pwalk)
{
   int move;

   while(grid[Y][X] != '#'){
      move = rand() % EDGES;
      switch(move){
         case 0:
            if(grid[Y][negmod(X + 1)] == '#'){
               grid[Y][X] = '#';
               break;
            }
            X = negmod(X + 1);
            break;
         case 1:
            if(grid[negmod(Y + 1)][X] == '#'){
               grid[Y][X] = '#';
               break;
            }
            Y = negmod(Y + 1);
            break;
         case 2:
            if(grid[Y][negmod(X - 1)] == '#'){
               grid[Y][X] = '#';
               break;
            }
            X = negmod(X - 1);
            break;
         case 3:
            if(grid[negmod(Y - 1)][X] == '#'){
               grid[Y][X] = '#';
               break;
            }
            Y = negmod(Y - 1);
            break;
         default:
            break;
      }
   }
}

/* Wrap out-of-bounds particles
   through modulus algorithm in form:
   (num % max_bound + max_bound) % max_bound */
int negmod(int tomod)
{
   int modded;

   modded = ((tomod) % GRIDLEN + GRIDLEN) % GRIDLEN;
   
   return(modded);
}
