/* Note: Structs implemented to test use of
   structs, might be more efficient/readable
   without in this case. */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "neillncurses.h"

#define GRIDLEN 50
#define EDGES 4
#define MAXPTCLES 250
#define Y pwalk->y
#define X pwalk->x

struct walker{
   int x;
   int y;
};
